#!/usr/bin/env sh

out=$1
test_switch(){
opam exec --switch="$1" -- dune exec ./analyses/gw_original.exe -- "$1" >> $out;
}

switches="4.14.1 5.0.0 5.1.0 5.1+gc_fixes 5.1+subarray_pacing 5.2.0+trunk"
rm $out
echo "    compiler version |   time | minor | major" > timings
for i in $switches
do
  echo $i;
  test_switch $i;
done
